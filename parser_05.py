import config as cfg
import logging
import struct

logging.lastResort.setLevel(logging.DEBUG)
logger = logging.getLogger(__name__)
if cfg.debug_parse:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

logger.info("Imported parser 05")
d_buffer = {}
d_keys = ['v', 'dg', 'rh', 'pa', 'ax', 'ay', 'az', 'pw', 'md', 'sq']

def parse(proc, line1):
    line2 = proc.stdout.readline()[:50].decode().rstrip().split()
    try:
        values = bytearray.fromhex("".join(line1 + line2))
    except BaseException as err:
        logger.error("something happened in parser:\n{}\n{}".format(line1, line2))
        raise

    logger.debug("raw: {}".format(values.hex()))
    d_values = struct.unpack(">chHHhhhHcHxxxxxx", values)
    d_dict = dict(zip(d_keys, d_values))
    d_dict['mv'] = (d_dict['pw'] >> 5) + 1600
    d_dict['db'] = (d_dict['pw'] & 0x001f) - 40
    d_dict['dg'] *= 0.005
    d_dict['rh'] *= 0.0025
    d_dict['pa'] += 5000
    d_dict['pa'] /= 100
    d_dict['ax'] /= 10
    d_dict['ay'] /= 10
    d_dict['az'] /= 10
    d_dict['md'] = d_dict['md'][0]
    del d_dict['v']
    del d_dict['pw']

    logger.debug("parsed: {}".format(d_dict))
    return d_dict
