import subprocess
import logging

logging.lastResort.setLevel(logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

logger.info("Imported bluetoothctl scan")

def collect():
    proc = subprocess.Popen(['unbuffer','bluetoothctl','scan','on'],
                            bufsize=1, stdout=subprocess.PIPE)
    return proc
