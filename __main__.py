#!/usr/bin/env python3

import time
import struct
import argparse
import logging

from importlib import import_module

import config as cfg

logging.lastResort.setLevel(logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

M_SCAN = 'bluetoothctl_scan'
M_STORE = 'sqlite_store'

SAMPLE_PERIOD = 60
# These are the tags we collect RSSI for
seen_tags = {}

T_KEY = ["ManufacturerData", "Key:"]
T_RUUVI = "0x0499"
T_RSSI = ["RSSI:"]

def main():
    logger.info("Listening for Ruuvi Tags")
    proc = m_scan.collect()

    starttime = int(time.time())
    cfg.period = int(starttime / SAMPLE_PERIOD) * SAMPLE_PERIOD
    indata = "x"
    while indata:
        ruuviparser = None
        indata = proc.stdout.readline().decode().rstrip().split()
        logger.debug("{}".format(indata))
        if indata[-3:-1] == T_KEY:
            if indata[-1] == T_RUUVI:
                logger.debug("{} {}".format(int(time.time()), cfg.period))
                d_mac = indata[2]
                seen_tags[d_mac] = cfg.period
                _ = proc.stdout.readline().decode().rstrip().split()
                line1 = proc.stdout.readline()[:50].decode().rstrip().split()
                values = bytearray.fromhex("".join(line1,))
                parser_name = "parser_{:02X}".format(values[0])
                try:
                    ruuviparser = import_module(parser_name)
                except ModuleNotFoundError as e:
                    logger.info("Parser not found: {}".format(parser_name))
                else:
                    try:
                        cfg.d_dict = ruuviparser.parse(proc, line1)
                    except BaseException as err:
                        logger.error("something happened in main: {}".format(line1))
                        raise
                    if cfg.period not in cfg.d_buffer:
                        logger.debug("create period {}".format(cfg.period))
                        cfg.d_buffer[cfg.period] = {}

                    if d_mac not in cfg.d_buffer[cfg.period]:
                        logger.debug("create d_mac {}".format(d_mac))
                        cfg.d_buffer[cfg.period][d_mac] = {}

                    if cfg.d_dict["sq"] not in cfg.d_buffer[cfg.period][d_mac]:
                        logger.debug("store data for sq {}".format(
                            cfg.d_dict["sq"]
                        ))
                        cfg.d_buffer[cfg.period][
                            d_mac][cfg.d_dict["sq"]] = cfg.d_dict

        logger.debug("Looking for rssi: {}".format(indata[-2:-1]))
        if indata[-2:-1] == T_RSSI:
            d_mac = indata[2]
            logger.debug("rssi mac {}".format(d_mac))
            logger.debug("seen tags {}".format(seen_tags.keys()))
            if d_mac in seen_tags.keys():
                logger.debug("rssi known mac {}".format(d_mac))
                if cfg.period not in cfg.d_rssi:
                    logger.debug("create period {} in rssi".format(cfg.period))
                    cfg.d_rssi[cfg.period] = {}

                if d_mac not in cfg.d_rssi[cfg.period]:
                    logger.debug("create d_mac {} in rssi".format(d_mac))
                    cfg.d_rssi[cfg.period][d_mac] = []

                logger.debug("store rssi {}".format(indata[-1]))
                cfg.d_rssi[cfg.period][d_mac].append(int(indata[-1]))

                logger.debug("\nrssi buffer: {}\n".format(cfg.d_rssi))

        logger.debug("\nbuffer: {}\n".format(cfg.d_buffer))
        if len(cfg.d_buffer) > 1:
            m_store.store()
      
        cfg.period = int(time.time() / SAMPLE_PERIOD) * SAMPLE_PERIOD
    logger.info("Ruuvi Tag logging exit")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--debug',
                        help='Comma separated, one or more of: '
                        'main, parser, scan, store')
    parser.add_argument('--scan',
                        help='Name of optional scan module')
    parser.add_argument('--store',
                        help='Name of optional store module')
    parser.add_argument('--dbname',
                        help='Name of database file')
    parser.add_argument('--period', type=int,
                        help='Sample period, in seconds')

    args = parser.parse_args()
    if args.period:
        # SAMPLE_PERIOD = int(args.period)
        SAMPLE_PERIOD = args.period
    if args.scan:
        M_SCAN = args.scan
    if args.store:
        M_STORE = args.store
    if args.dbname:
        cfg.db_name = args.dbname
    m_scan = import_module(M_SCAN)
    m_store = import_module(M_STORE)
    if args.debug:
        if "main" in args.debug:
            logger.setLevel(logging.DEBUG)
        if "scan" in args.debug:
            m_scan.logger.setLevel(logging.DEBUG)
        if "store" in args.debug:
            m_store.logger.setLevel(logging.DEBUG)
        if "parse" in args.debug:
            cfg.debug_parse = True
    main()
