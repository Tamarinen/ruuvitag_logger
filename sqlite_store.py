import logging
import config as cfg
import sqlite3

logging.lastResort.setLevel(logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

conn = sqlite3.connect(cfg.db_name)

c = conn.cursor()

# Create table
try:
    c.execute('''CREATE TABLE reports
    (device text, period int, key text, subkey text,
    value real, state text)''')
    conn.commit()
    logger.info("Created new database '{}'".format(cfg.db_name))
except sqlite3.OperationalError:
    logger.info("Using existing database '{}'".format(cfg.db_name))

logger.info("Imported sqlite store")

MASHKEYS = ("cnt", "max", "avg", "min")

def mash_list(d_list):
    logger.debug("d_list: {}".format(d_list))
    return dict(zip(MASHKEYS, (len(d_list), round(max(d_list), 4),
           round(sum(d_list) / len(d_list), 4),
                               round(min(d_list), 4))))

def store():
    logger.debug("store d_buffer: {}".format(cfg.d_buffer))
    buffer_keys = sorted(cfg.d_buffer.keys())
    logger.info("store period {}".format(buffer_keys[0]))
    logger.debug("\nkeys: {}\n".format(buffer_keys))
    logger.debug("\nbuffer: {}\n".format(cfg.d_buffer[buffer_keys[0]]))
    for data_mac in cfg.d_buffer[buffer_keys[0]].keys():
        prekey = "".join(data_mac.split(':')[-4:])
        mac_sqs = cfg.d_buffer[buffer_keys[0]][data_mac].keys()

        for sq_val in sorted(cfg.d_dict.keys()):
            d_list = []
            for mac_sq in mac_sqs:
                d_list.append(
                    cfg.d_buffer[buffer_keys[0]][data_mac][mac_sq][sq_val]
                )
            if sq_val not in ("sq",):
                mashed = mash_list(d_list)
                logger.debug("{} {}: {}".format(data_mac, sq_val, mashed))
                for mashkey in mashed.keys():
                    logger.debug("{} {} {} {}: {}".format(
                        data_mac, cfg.period, sq_val, mashkey, mashed[mashkey]
                    ))
                    c.execute("INSERT INTO reports VALUES "
                              "(?, ?, ?, ?, ?, 'NEW')",
                              (data_mac, cfg.period, sq_val,
                               mashkey, mashed[mashkey]))
    conn.commit()

    del cfg.d_buffer[buffer_keys[0]]

    if buffer_keys[0] in cfg.d_rssi.keys():
        logger.debug("rssi: {}\n".format(cfg.d_rssi[buffer_keys[0]]))
        rssi_macs = cfg.d_rssi[buffer_keys[0]].keys()
        for key in rssi_macs:
            rssis = cfg.d_rssi[buffer_keys[0]][key]
            prekey = "".join(key.split(':')[-4:])
            mashed = mash_list(rssis)
            logger.debug("{} rssi: {}".format(key, mashed))
            for mashkey in mashed.keys():
                logger.debug("{} {} {} {}: {}".format(
                    key, cfg.period, "rssi", mashkey, mashed[mashkey]
                ))
                c.execute("INSERT INTO reports VALUES "
                          "(?, ?, 'rssi', ?, ?, 'NEW')",
                          (key, cfg.period, mashkey, mashed[mashkey]))
        conn.commit()
        del cfg.d_rssi[buffer_keys[0]]
